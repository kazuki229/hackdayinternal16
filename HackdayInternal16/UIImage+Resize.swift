//
//  UIImage+Resize.swift
//  HackdayInternal16
//
//  Created by 都筑一希 on 2019/07/27.
//  Copyright © 2019 kazuki229. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    func resized(width: Int, height: Int) -> UIImage {
        guard width > 0 && height > 0 else {
            fatalError("Dimensions must be over 0.")
        }

        let newSize = CGSize(width: width, height: height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(origin: CGPoint.zero, size: newSize))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
