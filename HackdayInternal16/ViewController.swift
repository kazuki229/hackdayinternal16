//
//  ViewController.swift
//  HackdayInternal16
//
//  Created by 都筑一希 on 2019/07/27.
//  Copyright © 2019 kazuki229. All rights reserved.
//

import UIKit
import AVFoundation
import CoreML

class ViewController: UIViewController {
    // カメラデバイスそのものを管理するオブジェクトの作成
    // メインカメラの管理オブジェクトの作成
    var mainCamera: AVCaptureDevice?
    // インカメの管理オブジェクトの作成
    var innerCamera: AVCaptureDevice?
    // 現在使用しているカメラデバイスの管理オブジェクトの作成
    var currentDevice: AVCaptureDevice?

    // キャプチャーの出力データを受け付けるオブジェクト
    var photoOutput : AVCapturePhotoOutput?
    // キャプチャーの出力動画を受け付けるオブジェクト
    var videoDataOutput = AVCaptureVideoDataOutput()

    var captureSession = AVCaptureSession()
    // プレビュー表示用のレイヤ
    var cameraPreviewLayer : AVCaptureVideoPreviewLayer?

    var button: UIButton!
    var cancelButton: UIButton!

    var captureImageView: UIImageView!

    let hedMain = HED_fuse()
    let hedSO = HED_so()

    var count = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
//        setupButton()
        setupImageView()

        videoDataOutput.connections.first?.isCameraIntrinsicMatrixDeliveryEnabled = true
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.startRunning()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        DispatchQueue.global(qos: .userInitiated).async {
            self.captureSession.stopRunning()
        }
    }

    // カメラの画質の設定
    private func setupCaptureSession() {
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }

    // デバイスの設定
    private func setupDevice() {
        // カメラデバイスのプロパティ設定
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        // プロパティの条件を満たしたカメラデバイスの取得
        let devices = deviceDiscoverySession.devices

        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                mainCamera = device
            } else if device.position == AVCaptureDevice.Position.front {
                innerCamera = device
            }
        }
        // 起動時のカメラを設定
        currentDevice = mainCamera
//        currentDevice = AVCaptureDevice.default(for: AVMediaType.video)
        try! currentDevice!.lockForConfiguration()
        currentDevice!.activeVideoMinFrameDuration = CMTimeMake(value: 1, timescale: 30)// 1/30秒 (１秒間に30フレーム)
        currentDevice!.unlockForConfiguration()
    }

    // 入出力データの設定
    private func setupInputOutput() {
        let videoInput = try! AVCaptureDeviceInput.init(device: currentDevice!)
        captureSession.addInput(videoInput)

        videoDataOutput = AVCaptureVideoDataOutput()
        videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue.main)
        videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as AnyHashable as! String : Int(kCVPixelFormatType_32BGRA)]
        videoDataOutput.alwaysDiscardsLateVideoFrames = true
        captureSession.addOutput(videoDataOutput)
    }

    // カメラのプレビューを表示するレイヤの設定
    func setupPreviewLayer() {
        // 指定したAVCaptureSessionでプレビューレイヤを初期化
        self.cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        // プレビューレイヤが、カメラのキャプチャーを縦横比を維持した状態で、表示するように設定
        self.cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        // プレビューレイヤの表示の向きを設定
        self.cameraPreviewLayer?.connection?.videoOrientation = AVCaptureVideoOrientation.portrait

        self.cameraPreviewLayer?.frame = CGRect(x: 16, y: 30, width: view.frame.width-32, height: view.frame.width-32)

        self.view.layer.insertSublayer(self.cameraPreviewLayer!, at: 0)
    }

    private func doInferencePressed(image: UIImage) {
        let startDate = Date()

        // Convert our image to proper input format
        // In this case we need to feed pixel buffer which is 500x500 sized.
        let inputW = 500
        let inputH = 500
        guard let inputPixelBuffer = image.resized(width: inputW, height: inputH)
            .pixelBuffer(width: inputW, height: inputH) else {
                fatalError("Couldn't create pixel buffer.")
        }

        // Use different models based on what output we need
        let featureProvider: MLFeatureProvider
        featureProvider = try! hedMain.prediction(data: inputPixelBuffer)

        // Retrieve results
        guard let outputFeatures = featureProvider.featureValue(for: "upscore-fuse")?.multiArrayValue else {
            fatalError("Couldn't retrieve features")
        }

        // Calculate total buffer size by multiplying shape tensor's dimensions
        let bufferSize = outputFeatures.shape.lazy.map { $0.intValue }.reduce(1, { $0 * $1 })

        // Get data pointer to the buffer
        let dataPointer = UnsafeMutableBufferPointer(start: outputFeatures.dataPointer.assumingMemoryBound(to: Double.self),
                                                     count: bufferSize)

        // Prepare buffer for single-channel image result
        var imgData = [UInt8](repeating: 0, count: bufferSize)

        // Normalize result features by applying sigmoid to every pixel and convert to UInt8
        for i in 0..<inputW {
            for j in 0..<inputH {
                let idx = i * inputW + j
                let value = dataPointer[idx]

                let sigmoid = { (input: Double) -> Double in
                    return 1 / (1 + exp(-input))
                }

                let result = sigmoid(value)
                imgData[idx] = UInt8(result * 255)
            }
        }

        // Create single chanel gray-scale image out of our freshly-created buffer
        let cfbuffer = CFDataCreate(nil, &imgData, bufferSize)!
        let dataProvider = CGDataProvider(data: cfbuffer)!
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let cgImage = CGImage(width: inputW, height: inputH, bitsPerComponent: 8, bitsPerPixel: 8, bytesPerRow: inputW, space: colorSpace, bitmapInfo: [], provider: dataProvider, decode: nil, shouldInterpolate: true, intent: .defaultIntent)
        let resultImage = UIImage(cgImage: cgImage!)

        // Calculate the time of inference
        let endDate = Date()
        print("Inference is finished in \(endDate.timeIntervalSince(startDate))")

        self.captureImageView.image = resultImage
        self.captureImageView.alpha = 1
    }

    private func setupImageView() {
        captureImageView = UIImageView(frame: CGRect(x: 16, y: 400, width: view.frame.width-32, height: view.frame.width-32))
        captureImageView.backgroundColor = UIColor(displayP3Red: 255, green: 0, blue: 0, alpha: 0.2)
        captureImageView.contentMode = .scaleAspectFill
        captureImageView.clipsToBounds = true
        captureImageView.alpha = 1
        view.addSubview(captureImageView)
    }

    @objc private func deleteCaptureImage() {
        captureImageView.image = nil
        captureImageView.alpha = 0
    }
}

extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if count < 1 {
            count += 1
        } else {
            if Thread.isMainThread {
                doInferencePressed(image: sampleBuffer.toImage())
            } else {
                DispatchQueue.main.async {
                    self.doInferencePressed(image: sampleBuffer.toImage())
                }
            }
            count = 0
        }
    }
}
